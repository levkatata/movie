import { GeneralLayout } from "../generallayout/generallayout";
import CharCard from '../charcard';

export class CharactersLayout extends GeneralLayout {
    getDefaultUrl() {
        return "https://rickandmortyapi.com/api/character";
    }

    getItemsLayout() {
        return (
            <>
                {this.state.items.map((i) => {
                    return <CharCard key={i.id} 
                        item={i}/>;
                })}
            </>
        );
    }

}
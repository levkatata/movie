import { Link } from "react-router-dom";
import { CharInfoLayout } from "../charinfolayout/charinfolayout";
import styles from './locinfolayout.module.css';

export class LocInfoLayout extends CharInfoLayout {
    getDetailsLayout() {
        return (
            <div className={styles.divmain}>
                <div>Full location details</div>
                <div className={styles.divname}>{this.state.item.name}</div>
                <div className={styles.divtype}>{this.state.item.type}</div>
                <div className={styles.divdimension}>{this.state.item.dimension}</div>
                <div>Characters:</div>
                <div className={styles.divcharacters}>
                {
                    this.state.item.residents.map((r) => {
                        return <Link to="/pages/charinfo" state={{url: r}} style={{textDecoration: 'none'}}>
                            <button>{r.match(/\d/g)}</button>
                            </Link>;
                    })
                }
                </div>
            </div>
        );
    }
}
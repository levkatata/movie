import { useState } from 'react';
import MainLogo from '../../images/home.png';
import styles from './homelayout.module.css';

export function HomeLayout() {
    const [filterValue, setFilterValue] = useState("");

    const onChangeFilterValue = (e) => {
        setFilterValue(e.target.value);
    }

    return (<div className={styles.divhome}>
        <img src={MainLogo}/>
        <input type="text"
            placeholder='Filter by name or episode (ex. S01 or S01E02)'
            value={filterValue}
            onChange={onChangeFilterValue}/>
    </div>);
}
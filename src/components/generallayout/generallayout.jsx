import { Component } from 'react';
import styles from './generallayout.module.css';
import LoadingIcon from '../../images/loading-icon.jpg';
import Pagination from '../pagination';

export class GeneralLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            items: [],
            prevUrl: null,
            nextUrl: null
        }
    }

    componentDidMount() {
        this.loadData(this.getDefaultUrl());
    }

    render() {
        return (<div className={styles.divmain}>
            {!this.state.isLoaded ? 
            <img className={styles.imgloading} src={LoadingIcon}/> 
            :
            <>
                <div className={styles.divitems}>
                    {this.getItemsLayout()}
                </div>
                <Pagination onClickNext={this.onClickNext} onClickPrev={this.onClickPrev}/>
            </>
            }
        </div>);
    }

    loadData = (url) => {
        fetch(url)
            .then((res) => res.json())
            .then((response) => {
                this.setState((base) => {
                    return {...base,
                        isLoaded: true,
                        prevUrl: response.info.prev,
                        nextUrl: response.info.next,
                        items: response.results    
                    };
                });
            },
            (error) => {
                console.error(error);
                this.setLoading(true);
            });
    }


    setLoading = (isLoaded) => {
        this.setState((base) => {return {...base, isLoaded: isLoaded}});
    }
    
    onClickNext = () => {
        if (this.state.nextUrl !== null && this.state.isLoaded) {
            this.setState((base) => {
                return {...base, curUrl: this.state.nextUrl, isLoaded: false, chars: []};
            });
            this.loadData(this.state.nextUrl);
        }
    }
    onClickPrev = () => {
        if (this.state.prevUrl !== null && this.state.isLoaded) {
            this.setState((base) => {
                return {...base, curUrl: this.state.prevUrl, isLoaded: false, chars: []};
            });
            this.loadData(this.state.prevUrl);
        }
    }
}
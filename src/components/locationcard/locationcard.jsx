import { Component } from "react";
import { Link } from "react-router-dom";
import styles from './locationcard.module.css';

export class LocationCard extends Component {
    render() {
        return (
            <Link to="/pages/pagelocation" state={{ item: this.props.item}} 
                style={{ textDecoration: 'none' }}>
                <div className={styles.divmain}>
                    <div className={styles.divname}>{this.props.item.name}</div>
                    <div className={styles.divtype}>{this.props.item.type}</div>
                    <div className={styles.divdimension}>{this.props.item.dimension}</div>
                </div>
            </Link>
        );
    }
}
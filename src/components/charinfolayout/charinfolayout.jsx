import { Component } from "react";
import styles from './charinfolayout.module.css';
import LoadingIcon from '../../images/loading-icon.jpg';
import { Link } from "react-router-dom";

export class CharInfoLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item || undefined,
            isLoaded: (this.props.item !== undefined) ? true : false
        }
    }

    componentDidMount() {
        if (!this.state.isLoaded) {
            fetch(this.props.url)
                .then((res) => res.json())
                .then((response) => {
                    this.setState((base) => {
                        return {...base,
                            isLoaded: true,
                            item: response    
                        };
                    });
                },
                (error) => {
                    console.error(error);
                    this.setLoading(true);
                });
        }
    }

    render() {
        return (
            <div className={styles.divmain}>
            {   (this.state.isLoaded === true) 
                ? 
                (this.getDetailsLayout())
                :
                <img className={styles.imgloading} src={LoadingIcon}/>
            }
        </div>
        );
    }

    getDetailsLayout() {
        return (<>
            <img className={styles.imgcharacter} src={this.state.item.image} />
            <div className={styles.divname}>{this.state.item.name}</div>
            <div className={styles.divspecies}>{this.state.item.species}</div>
            <div className={styles.divgender}>Gender: {this.state.item.gender}</div>
            <div className={styles.divstatus}>Status: {this.state.item.status}</div>
            <div>Episodes:</div>
            <div className={styles.listepisodes}>
                {
                    this.state.item.episode.map((e) => {
                        return <Link to={'/pages/pageepisode'} state={{ url : e}} style={{ textDecoration: 'none' }}>
                                <button>
                                {e.match(/\d{1,}/g)}
                                </button>
                            </Link>;
                    })
                }
            </div>
            <div className={styles.divlocation}>
                <div>Location:</div>
                <div>{this.state.item.location.name}</div>
                <button>
                    <Link to={'/pages/pagelocation'} state={{url : this.state.item.location.url}} 
                        style={{ textDecoration: 'none', color: "black" }}>
                        About...
                    </Link>
                </button>
            </div>
            </>);
    }

}
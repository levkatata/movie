import { Link } from "react-router-dom";
import { CharInfoLayout } from "../charinfolayout/charinfolayout";
import styles from './episodeinfolayout.module.css';

export class EpisodeInfoLayout extends CharInfoLayout {
    getDetailsLayout() {
        return (<div className={styles.divmain}>
            <div>Full episode details</div>
            <div className={styles.divname}>{this.state.item.name}</div>
            <div className={styles.divair_date}>{this.state.item.air_date}</div>
            <div className={styles.divepisode}>{this.state.item.episode}</div>
            <div className={styles.divcreated}>{this.state.item.created}</div>
            <div>Characters:</div>
            <div className={styles.divcharacters}>
            {
                this.state.item.characters.map((c) => {
                    return <Link to={"/pages/charinfo"} state={{url: c}} style={{textDecoration: 'none'}}>
                            <button>{c.match(/\d/g)}</button>
                        </Link>;
                })
            }
            </div>
        </div>);
    }
}
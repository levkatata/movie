import { Link } from 'react-router-dom';
import styles from './charcard.module.css';

export function CharCard(props) {
    return (
        <Link to={'/pages/charinfo'} state={{ item : props.item}} style={{ textDecoration: 'none' }}>
            <div className={styles.divcard} id={props.item.id}>
                    <img src={props.item.image}/>
                    <div>{props.item.name}</div>
                    <div>{props.item.species}</div>
            </div>
        </Link>
    );
}
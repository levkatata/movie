import { Component } from "react";
import { Link } from "react-router-dom";
import Logo from '../../images/logo.png';
import styles from './header.module.css';

export const options = ['Characters', 'Episodes', 'Locations'];

export class Header extends Component {
    render() {
        return (<div className={styles.divheader}>
            <Link to={'/'}><img src={Logo}/></Link>
            <ul>
                <li className={this.props.active === options[0] ? styles.active : ""}>
                    <Link to={'/pages/characters'}>{options[0]}</Link>
                </li>
                <li className={this.props.active === options[1] ? styles.active : ""}>
                    <Link to={'/pages/episodes'}>{options[1]}</Link>
                </li>
                <li className={this.props.active === options[2] ? styles.active : ""}>
                    <Link to={'/pages/locations'}>{options[2]}</Link>
                </li>
           </ul>
        </div>);
    }
}
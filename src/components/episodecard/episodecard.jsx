import { Component } from "react";
import styles from './episodecard.module.css';
import { Link } from "react-router-dom";

export default class EpisodeCard extends Component {
    render() {
        return (
            <Link to={'/pages/pageepisode'} state={{ item : this.props.item}} style={{ textDecoration: 'none' }}>
                <div className={styles.divmain}>
                    <div className={styles.divname}>{this.props.item.name}</div>
                    <div className={styles.divair_date}>{this.props.item.air_date}</div>
                    <div className={styles.divepisode}>{this.props.item.episode}</div>
                </div>
            </Link>
        );
    }
}
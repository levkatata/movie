import styles from './pagination.module.css';
import ArrowRight from '../../images/arrow-right.jpg';
import ArrowLeft from '../../images/arrow-left.jpg';

export function Pagination(props) {
    return (
        <div className={styles.divpanel}>
            <img src={ArrowLeft} onClick={props.onClickPrev}/>
            <img src={ArrowRight} onClick={props.onClickNext}/>
        </div>
    );
}
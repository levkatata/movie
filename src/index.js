import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Home } from './pages/home';
import { Characters } from './pages/characters';
import { Episodes } from './pages/episodes';
import './index.css';
import { Locations } from './pages/locations';
import { NoPage } from './pages/nopage';
import { PageLocation } from './pages/pagelocation';
import { PageCharInfo } from './pages/pagecharinfo';
import { PageEpisode } from './pages/pageepisode';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/pages/characters" element={<Characters/>}/>
      <Route path="/pages/episodes" element={<Episodes/>}/>
      <Route path="/pages/locations" element={<Locations/>}/>
      <Route path="*" element={<NoPage/>}/>
      <Route path="/pages/charinfo" element={<PageCharInfo/>}/>
      <Route path="/pages/pagelocation" element={<PageLocation/>}/>
      <Route path="/pages/pageepisode" element={<PageEpisode/>}/>
    </Routes>
  </BrowserRouter>
);

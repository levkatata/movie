import { Component } from "react";
import { EpisodesLayout } from "../components/episodeslayout/episodeslayout";
import { Header, options } from "../components/header/header";

export class Episodes extends Component {
    render() {
        return (
            <>
                <Header  active={options[1]}/>
                <EpisodesLayout/>
            </>
        )
    }
}
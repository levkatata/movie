import { useLocation } from "react-router-dom";
import { CharInfoLayout } from "../components/charinfolayout/charinfolayout";

export function PageCharInfo() {
    const location = useLocation();
    const { item, url } = location.state;
    return <CharInfoLayout item={item} url={url}/>
}
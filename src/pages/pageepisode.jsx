import { useLocation } from "react-router-dom";
import { EpisodeInfoLayout } from "../components/episodeinfolayout/episodeinfolayout";

export function PageEpisode() {
    const location = useLocation();
    const {item, url} = location.state;
    return <EpisodeInfoLayout item={item} url={url}/>
}
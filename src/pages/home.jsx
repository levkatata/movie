import { Component } from "react";
import { Header } from "../components/header/header";
import { HomeLayout } from "../components/homelayout/homelayout";

export class Home extends Component {
    render() {
        return (
            <>
                <Header items={this.props.items}/>
                <HomeLayout />
            </>
        )
    }
}
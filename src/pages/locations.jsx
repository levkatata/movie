import { Component } from "react";
import { Header, options } from "../components/header/header";
import { LocationsLayout } from "../components/locationslayout/locationslayout";

export class Locations extends Component {
    render() {
        return (
            <>
                <Header active={options[2]}/>
                <LocationsLayout/>
            </>
        )
    }
}
import { Component } from "react";
import { CharactersLayout } from "../components/characteslayout/characteslayout";
import { Header, options } from "../components/header/header";

export class Characters extends Component {
    render() {
        return (
            <>
                <Header active={options[0]}/>
                <CharactersLayout />
            </>
        )
    }
}
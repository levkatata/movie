import { useLocation } from "react-router-dom"
import { LocInfoLayout } from "../components/locinfolayout/locinfolayout";

export function PageLocation() {
    const location = useLocation();
    const { item, url} = location.state;
    return <LocInfoLayout item={item} url={url}/>;
}